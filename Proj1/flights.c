/* 
 * CS61C Summer 2013
 * Name:
 * Login:
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flights.h"
#include "timeHM.h"

// should everything be pointers so that everything are stored in heap????

static void allocation_failed() {
  fprintf(stderr, "Out of memory.\n");
  exit(EXIT_FAILURE);
}


typedef struct arrayList {
    // Begin size
    unsigned int arrayLen;
    unsigned int sizeOfList;
    unsigned int sizePerPointer;
    unsigned int resizeRate;

    // pointer to an array
    void **array;
} list;


list *initList(int sizePerPointer) {
  list *l = malloc(sizeof(struct arrayList));
  if (!l) { allocation_failed(); }
  l->arrayLen = 8;
  l->sizeOfList = 0;
  l->resizeRate = 2;
  l->sizePerPointer = sizePerPointer;
  l->array = calloc(l->arrayLen, l->sizePerPointer);
  if (!l->array) {
    free(l);
    allocation_failed();
  }
  return l;
}


// test method, print integer
void print(list *l) {
  for (int i = 0; i < l->sizeOfList; i++) {
    int *p = l->array[i];
    printf("%d ", *p);
    printf("\n");
  }
}

// seems useless since can't overwrite free....
void deleteList(list *l) {
  for (int i = 0; i < l->sizeOfList; i++) {
    free(l->array[i]);
  }
  free(l);
}

struct flightSys {
    // Place the members you think are necessary for the flightSys struct here.
    list *airports;
};


// all airports, flights should be stored in heap
struct airport {
    // Place the members you think are necessary for the airport struct here.
    list *flights;
    char *name;
};

struct flight {
    // Place the members you think are necessary for the flight struct here.
    timeHM_t dep;
    timeHM_t arr;
    int cost;
    airport_t *dest;
};

//free flight (don't free the airport pointer in flight). Should only called by freeAirport ?
//                                                        (Assuming every flight is stored in one airport?)
void freeFlight(flight_t *f) {
  if (f) {
    free(f);
  }
}

void freeAirport(airport_t *airport) {
  if (airport == NULL) {
    return;
  }
  if(airport->name) { free(airport->name); }
  list *l = airport->flights;

  for (int i = 0; i < l->sizeOfList; i++) {
    freeFlight(l->array[i]);
  }
  free(l->array);
  free(l);
  free(airport);
}

// add pointer, p should pointing somewhere inside heap
void add(list *l, void *p) {
  // type & null pointer check
  if (!l) {
    printf("list* is null\n");
    exit(1);
  }

  if (sizeof(p) != l->sizePerPointer) {
    printf("invalid addition in arrayList\n");
    exit(1);
  }

  // add to array directly
  if (l->sizeOfList + 1 <= l->arrayLen) {
    l->array[l->sizeOfList] = p;
    l->sizeOfList++;
  } else {
    l->array = realloc(l->array, (l->arrayLen *= l->resizeRate) * l->sizePerPointer);
    if (!l->array) {
      allocation_failed();
    }
    l->array[l->sizeOfList] = p;
    l->sizeOfList++;
  }
}

/*
   Creates and initializes a flight system, which stores the flight schedules of several airports.
   Returns a pointer to the system created.
 */
flightSys_t *createSystem() {
  // Replace this line with your code
  flightSys_t *sys = malloc(sizeof(flightSys_t));
  if (!sys) { allocation_failed(); }
  sys->airports = initList(sizeof(airport_t *));
  return sys;
}


/* Given a destination airport, a departure and arrival time, and a cost, return a pointer to new flight_t. Note that this pointer must be available to use even
   after this function returns. (What does this mean in terms of how this pointer should be instantiated)?
*/

flight_t *createFlight(airport_t *dest, timeHM_t dep, timeHM_t arr, int c) {
  // Replace this line with your code
  // flight pointer
  // to make sure everything stored in the heap
  flight_t *fp = malloc(sizeof(flight_t));
  if (!fp) { allocation_failed(); }
  fp->dest = dest;
  fp->dep = dep;
  fp->arr = arr;
  fp->cost = c;
  return fp;
}

/*
   Frees all memory associated with this system; that's all memory you dynamically allocated in your code.
 */
void deleteSystem(flightSys_t *s) {
  // Replace this line with your code
  if (s == NULL) {
    printf("try to delete null system");
    return;
  }

  list *l = s->airports;
  for (int i = 0; i < l->sizeOfList; i++) {
    freeAirport(l->array[i]);
  }
  free(l->array);
  free(l);
  free(s);
}


/*
   Adds a airport wi  th the given name to the system. You must copy the string and store it.
   Do not store "name" (the pointer) as the contents it point to may change.
 */
void addAirport(flightSys_t *s, char *name) {
  // Replace this line with your code
  if (s == NULL) {
    printf("trying add airport to null system");
    return;
  }

  airport_t *airport = malloc(sizeof(airport_t));
  if (airport == NULL) { allocation_failed(); }

  // +2 to make sure there is space for '\0'
  if (name) {
    airport->name = malloc((strlen(name) + 2) * sizeof(char));
    if (!airport->name) {
      free(airport);
      allocation_failed();
    }
    strcpy(airport->name, name);
  } else {
    airport->name = NULL;
  }

  airport->flights = initList(sizeof(flight_t *));
  add(s->airports, airport);

}


/*
   Returns a pointer to the airport with the given name.
   If the airport doesn't exist, return NULL.
 */
airport_t *getAirport(flightSys_t *s, char *name) {
  // Replace this line with your code
  if (s == NULL) {
    printf("trying get airport from a null system");
    return NULL;
  }

  if (name == NULL) {
    printf("trying to get a airport but the given name is null");
    return NULL;
  }

  list *l = s->airports;
  for (int i = 0; i < l->sizeOfList; i++) {
    airport_t *airport = l->array[i];
    if(airport->name == NULL) { printf("NULL name airport\n"); return NULL;}

    if (strcmp(airport->name, name) == 0) {
      return airport;
    }
  }
  return NULL;
}


/*
   Print each airport name in the order they were added through addAirport, one on each line.
   Make sure to end with a new line. You should compare your output with the correct output
   in flights.out to make sure your formatting is correct.
 */
void printAirports(flightSys_t *s) {
  // Replace this line with your code
  if (s == NULL) {
    printf("trying to print a null system\n");
    return;
  }
  list *l = s->airports;
  for (int i = 0; i < l->sizeOfList; i++) {
    airport_t *p = l->array[i];
    char *name = p->name;
    if(name == NULL) {
      printf("null name in airpor\nt");
      return;
    }
    printf("%s\n", name);
  }

}


/*
   Adds a flight to src's schedule, stating a flight will leave to dst at departure time and arrive at arrival time.
 */
void addFlight(airport_t *src, airport_t *dst, timeHM_t *departure, timeHM_t *arrival, int cost) {
  // Replace this line with your code
  if (src == NULL || departure == NULL || arrival == NULL) {
    printf("Null input when addFlight\n");
    return;
  }

  list *l = src->flights;
  flight_t *f = createFlight(dst, *departure, *arrival, cost);
  add(l, f);
}


/*
   Prints the schedule of flights of the given airport.

   Prints the airport name on the first line, then prints a schedule entry on each 
   line that follows, with the format: "destination_name departure_time arrival_time $cost_of_flight".

   You should use printTime (look in timeHM.h) to print times, and the order should be the same as 
   the order they were added in through addFlight. Make sure to end with a new line.
   You should compare your output with the correct output in flights.out to make sure your formatting is correct.
 */
void printSchedule(airport_t *s) {
  // Replace this line with your code

  if (s == NULL) {
    printf("A null airport doesn't have a schedule\n");
    return;
  }

  printf("%s", s->name);
  printf("\n");
  list *lptr = s->flights;
  void **flPtr = lptr->array;
  int len = lptr->sizeOfList;
  int i;
  for (i = 0; i < len; i++) {
    flight_t *fly = flPtr[i];
    airport_t *dest = fly->dest;

    if(dest == NULL || dest->name == NULL) {
      return;
    }

    printf("%s", dest->name);
    printf("%s", " ");
    printTime(&(fly->dep));
    printf("%s", " ");
    printTime(&(fly->arr));
    printf("%s", " $");
    printf("%d", fly->cost);
    printf("%s", "\n");
  }
}


/*
   Given a src and dst airport, and the time now, finds the next flight to take based on the following rules:
   1) Finds the cheapest flight from src to dst that departs after now.
   2) If there are multiple cheapest flights, take the one that arrives the earliest.

   If a flight is found, you should store the flight's departure time, arrival time, and cost in departure, arrival, 
   and cost params and return true. Otherwise, return false. 

   Please use the function isAfter() from time.h when comparing two timeHM_t objects.
 */
bool getNextFlight(airport_t *src, airport_t *dst, timeHM_t *now, timeHM_t *departure, timeHM_t *arrival, int *cost) {
  // Replace this line with your code
  if (src == NULL || dst == NULL || departure == NULL || arrival == NULL || now == NULL || cost == NULL) {
    printf("Invalid input when getNextFlight\n");
    return false;
  }

  void **array = src->flights->array;
  flight_t *flPtr = NULL;
  int len = src->flights->sizeOfList;
  int i;
  for (i = 0; i < len; i++) {
    flight_t *temp = *(array + i);
    if (temp->dest == dst && isAfter(&(temp->dep), now)) {
      if (flPtr == NULL ||
          (temp->cost < flPtr->cost) ||
          (temp->cost == flPtr->cost && isAfter(&(flPtr->arr), &(temp->arr)))) {
        flPtr = temp;
      }
    }//if
  }//for
  if (flPtr) {
    *departure = flPtr->dep;
    *arrival = flPtr->arr;
    *cost = flPtr->cost;
    return true;
  }
  return false;
}//getNextFlight


/* Given a list of flight_t pointers (flight_list) and a list of destination airport names (airport_name_list),
 * first confirm that it is indeed possible to take these sequences of flights,
   (i.e. be sure that the i+1th flight departs after or at the same time as the ith flight arrives) (HINT: use the isAfter and isEqual functions).
   Then confirm that the list of destination airport names match the actual destination airport names of the provided flight_t struct's.
   sz tells you the number of flights and destination airport names to consider. Be sure to extensively test for errors
   (i.e. if you encounter NULL's for any values that you might expect to
   be non-NULL, return -1).

   Return from this function the total cost of taking these sequence of flights. If it is impossible to take these sequence of flights, if the list of destination airport names
   doesn't match the actual destination airport names provided in the flight_t struct's, or if you run into any errors mentioned previously or any other errors,
    return -1.
*/
int validateFlightPath(flight_t **flight_list, char **airport_name_list, int sz) {
  // Replace this line with your code

  // null check
  if (!flight_list || !airport_name_list) { return -1; }
  for(int j = 0; j < sz; j++) {
    if(flight_list[j] == NULL || airport_name_list[j] == NULL) { return -1; }
    airport_t *airport = flight_list[j]->dest;
    if(airport == NULL) { return -1; }
    if(airport->name == NULL) { return -1; }
  }
  // end of null check

  int total_cost = 0;
  int i;
  for (i = 0; i < sz - 1; i++) {
    airport_t *airport = flight_list[i]->dest;
    if (isAfter(&(flight_list[i]->arr), &(flight_list[i + 1]->dep)) ||
        !isEqual(&(flight_list[i]->arr), &(flight_list[i + 1]->dep)) ||
        strcmp(airport->name, airport_name_list[i])) {
      return -1;
    }
    total_cost += (flight_list[i]->cost);
  }

  return total_cost += (flight_list[i]->cost);
}

